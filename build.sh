#! /bin/bash

declare -A APPS
APPS[core]=""
APPS[fc]=""
APPS[mail]=""

if [ "$1" = "all" ]
then
    apps_to_recompile="${!APPS[@]}"
else
    apps_to_recompile="$@"
fi

#find path to apps
for app in ${apps_to_recompile}
do
    if [ ! -d "${APPS[$app]}" ]
    then
        read -p "Provide directory path to ${app} app: " APPS[$app]
    fi
done

#build app and create docker image
for app in ${apps_to_recompile}
do
    echo "building ${app}"
    cd ${APPS[$app]}
    mvn package
    mvn install dockerfile:build
    cd -
done

#create reverse proxy docker image
docker build -t reverseproxy .
